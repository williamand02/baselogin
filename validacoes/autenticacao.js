module.exports = function(req, res) {
	req.assert('email', "E-mail inválido").isEmail();
	req.assert('password', 'Sua senha deve conter no minimo 6 caracter e no maximo 10.').len(6, 10);

	var validateErros = req.validationErrors() || [];

	if (validateErros.length > 0) {
		validateErros.forEach(function(e) {
			req.flash('erro', e.msg);
		})
		return false;
	} else {
		return true;
	}
}