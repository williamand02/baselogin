module.exports = function(req, res) {
	req.assert('req.body.nome', 'Informe seu nome').notEmpty();
	if (req.body.email != '') {
		req.assert('req.body.email', 'E-mail inválido.').isEmail();
		var validacoesErros = req.validationErrors() || [];
		if (validacoesErros.lengyh > 0) {
			validacoesErros.forEach(function(e) {
				req.flash('erro', e.msg);
			});
			return false;
		}
		return true;
	}
}