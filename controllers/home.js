var nodemailer = require('nodemailer');

module.exports = function(app) {
		var Usuario = app.models.usuarios;
		var validacao = require('../validacoes/autenticacao');
		var HomeController = {
			index: function(req, res) {
				res.render('home/index');
			},
			login: function(req, res) {
				res.render('home/login');
			},
			autenticacao: function(req, res) {
				var usuario = new Usuario();
				var email = req.body.email;
				var password = req.body.password;

			if (validacao(req, res)) {
				Usuario.findOne({'email': email}, function(err, data) {
					console.log("senha da tela: "+password)
					console.log("senha do banco: "+data.password)
					// console.log(data)
						if (err) {
							console.log("entrou no erro controllers home ")
							req.flash('erro', 'Erro ao entrar no sistema: ' + err);
							res.redirect('/');
						} else if (!data) {
							console.log("entrou no erro email controllers home ")
							req.flash('erro', 'E-mail não encontrado');
							res.redirect('/');

						} else if (!usuario.validPassword(password, data.password)) {
							console.log("entrou no erro senha controllers home ")
							req.flash('erro', 'Senha não confere!');
							res.redirect('/');
						} else {
							console.log("entrou no else  controllers home ")
							req.session.usuario = data;
							res.redirect('/home');
						}
					});

				}else{
					res.redirect('/');

				}
			},
			logout: function(req,res){
				req.session.destroy();
				res.redirect('/');
			},
			email: function(req,res){
				res.render('home/email')
			},
			enviar: function(req, res) {
				// EMAIL NÃO ESTA FUNCIONANDO

				var transport = nodemailer.createTransport({
					service: "gmail",
					host: "smtp.gmail.com",
					// host: 'smtp.mandrillapp.com',
					port: 465,
					auth: {
						user: 'williamstrayder@gmail.com',
						pass: 'Will-32060677'
					}
				});
				var mailOptions = {
					from: req.body.nome + " <" + req.body.email + ">",
					to: 'williamstrayder@gmail.com',
					subject: req.body.assunto,
					text: req.body.mensagem
				};
				transport.sendMail(mailOptions, function(err, response) {
					if (err) {
						req.flash('erro', 'Erro ao enviar e-mail' + err);
						res.redirect('/email');
					}
					req.flash('info', 'E-mail enviado som sucesso');
					res.render('home/email');
				});
			}
		}
			return HomeController;
		}